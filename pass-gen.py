import csv

char_num = 8

# alphabet_list = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
num_list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
# mark_list = ['!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '-', '~', '`', '?', '<', '>']

char_list = list()
char_list.extend(num_list)

def gen():
	i_num = 1
	tab_num = 1
	tab = '\t'
	text_code = "with open('pass_list.csv', mode='a') as csv_file:\n"
	text_code += f"{tab_num*tab}pl = csv.writer(csv_file, delimiter=',')\n"
	for n in range(char_num):
		text_code += f"{tab_num*tab}for {i_num*'i'} in range(len(char_list)):\n"
		i_num += 1
		tab_num += 1
	text_code += f"{tab_num*tab}pl.writerow(["
	i_num = 1
	for n in range(char_num):
		if n == char_num - 1:
			text_code += f"char_list[{i_num*'i'}]"
		else:
			text_code += f"char_list[{i_num*'i'}] + "
			i_num += 1
	text_code += "])"
	# exec(text_code)
	print(text_code)
	
name = ['test', '1234', '5678']

def gen2():
	with open('pass_list.csv', mode='a') as csv_file:
		pl = csv.writer(csv_file, delimiter=',')
		for i in range(len(name)):
			for j in range(len(name)):
				if (len(name[i]) + len(name[j]) >= 8 and i!=j):
					pl.writerow([name[i] + name[j]])
		for i in range(len(name)):
			for j in range(len(name)):
				for k in range(len(name)):
					if (len(name[i]) + len(name[j]) + len(name[k]) >= 8 and i!=j!=k):
						pl.writerow([name[i] + name[j] + name[k]])

def main():
	# gen()
	gen2()				

main()